from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from app.models import *
from app.views import *
from rest_framework import viewsets, routers

class UserViewSet(viewsets.ModelViewSet):
	model = User
class UsuarioViewSet(viewsets.ModelViewSet):
	model = Usuario
class EscuelaViewSet(viewsets.ModelViewSet):
	model = Escuela
class MaestroViewSet(viewsets.ModelViewSet):
	model = Maestro
class MateriaViewSet(viewsets.ModelViewSet):
	model = Materia
class ImagenViewSet(viewsets.ModelViewSet):
	model = Imagen
class RecursoViewSet(viewsets.ModelViewSet):
	model = Recurso
class ClaseViewSet(viewsets.ModelViewSet):
	model = Clase

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'usuarios', UsuarioViewSet)
router.register(r'escuelas', EscuelaViewSet)
router.register(r'maestros', MaestroViewSet)
router.register(r'materias', MateriaViewSet)
router.register(r'imagenes', ImagenViewSet)
router.register(r'recursos', RecursoViewSet)
router.register(r'clases', ClaseViewSet)

router.register(r'solicitudes', SolicitudViewSet)
router.register(r'opciones', OpcionViewSet)

urlpatterns = patterns('',
	url(r'^$', 'app.views.home', name='home'),
	url(r'^proximamente$', 'app.views.proximamente', name="proximamente"),
	url(r'^curso/emprender$', 'app.views.curso', name="curso"),

	url(r'^api/', include(router.urls)),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


	url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
