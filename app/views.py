from django.shortcuts import render
from app.models import *

def home(req):
	return render(req, 'home.html')
def proximamente(req):
	return render(req, 'prox.html')
def curso(req):
	return render(req, 'curso.html')



from app.serializers import *
from rest_framework import viewsets
from rest_framework import permissions

class SolicitudViewSet(viewsets.ModelViewSet):
	queryset = Solicitud.objects.all()
	serializer_class = SolicitudSerializer 
	permission_classes = (permissions.AllowAny,)

class OpcionViewSet(viewsets.ModelViewSet):
	queryset = Opcion.objects.all()
	serializer_class = OpcionSerializer 
	permission_classes = (permissions.AllowAny,)