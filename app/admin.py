from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import User

from app.models import *

class UserProfileInline(admin.TabularInline):
    model = Usuario

class UserAdmin(DjangoUserAdmin):
    inlines = (UserProfileInline,)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)


class EscuelaAdmin(admin.ModelAdmin):
	list_display = ('nombre',)

class MaestroAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'apellido_paterno', 'apellido_materno',)
	filter_horizontal = ('escuela',)

class MateriaAdmin(admin.ModelAdmin):
	list_display = ('nombre',)

class ImagenAdmin(admin.ModelAdmin):
	list_display = ('imagen_show',)
	def imagen_show(self, obj):
		return '<img src="%s" width="70" />' % obj.imagen
	imagen_show.allow_tags = True

class RecursoAdmin(admin.ModelAdmin):
	list_display = ('titulo', 'tipo_recurso', 'url',)

class ClaseAdmin(admin.ModelAdmin):
	list_display = ('imagen_show', 'materia', 'maestro', 'tema', 'descripcion',)
	filter_horizontal = ('recursos',)
	def imagen_show(self, obj):
		return '<img src="%s" width="70" />' % obj.imagen
	imagen_show.allow_tags = True

class SolicitudAdmin(admin.ModelAdmin):
	list_display = ('email',)
class OpcionAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'votos',)

admin.site.register(Escuela, EscuelaAdmin)
admin.site.register(Maestro, MaestroAdmin)
admin.site.register(Materia, MateriaAdmin)
admin.site.register(Imagen, ImagenAdmin)
admin.site.register(Recurso, RecursoAdmin)
admin.site.register(Clase, ClaseAdmin)
admin.site.register(Solicitud, SolicitudAdmin)
admin.site.register(Opcion, OpcionAdmin)