from rest_framework import serializers
from app.models import *

class SolicitudSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Solicitud
		fields = ('email',)

class OpcionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Opcion
		fields = ('nombre', 'votos', 'id',)