from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.db import models


class Usuario(models.Model):
	avatar = models.ImageField(upload_to="users/avatar/")
	edad = models.IntegerField(default=0)
	creditos = models.IntegerField(default=0)
	facebook_id = models.CharField(max_length=140, blank=True)
	twitter_id = models.CharField(max_length=140, blank=True)
	user = models.OneToOneField(User, unique=True)
	def __unicode__(self):
		return self.user.username
def create_profile(sender, instance, created, **kwargs):
	if created:
		profile = Usuario()
		profile.user = instance
		profile.save()
post_save.connect(create_profile, sender=User)

class Escuela(models.Model):
	nombre = models.CharField(max_length=140)
	def __unicode__(self):
		return self.nombre

class Maestro(models.Model):
	nombre = models.CharField(max_length=140)
	apellido_paterno = models.CharField(max_length=140)
	apellido_materno = models.CharField(max_length=140)
	escuela = models.ManyToManyField(Escuela)
	def __unicode__(self):
		return '%s %s %s %s' % (self.nombre, self.apellido_paterno, self.apellido_materno, self.escuela)

class Materia(models.Model):
	nombre = models.CharField(max_length=140)
	def __unicode__(self):
		return self.nombre


class Imagen(models.Model):
	imagen = models.ImageField(upload_to="imagenes/")
	def __unicode__(self):
		return self.imagen
	class Meta:
		verbose_name_plural = "Imagenes"

class Recurso(models.Model):
	titulo = models.CharField(max_length=140)
	url = models.URLField()
	TIPO_RECURSO = (
		('video', 'Video'),
		('pdf', 'PDF'),
		('word', 'Word'),
		('texto', 'Texto'),
	)
	tipo_recurso = models.CharField(max_length="10", choices=TIPO_RECURSO, default="video")
	class Meta:
		verbose_name_plural = "Recursos"

class Clase(models.Model):
	materia = models.ForeignKey(Materia)
	maestro = models.ForeignKey(Maestro)
	imagen = models.ForeignKey(Imagen)
	tema = models.CharField(max_length=140)
	descripcion = models.TextField()
	semestre = models.IntegerField(default=1)
	votos = models.IntegerField(default=0)
	visitas = models.IntegerField(default=0)
	recursos = models.ManyToManyField(Recurso)

class Solicitud(models.Model):
	email = models.EmailField(unique=True)
	class Meta:
		verbose_name_plural = "Solicitudes"

class Opcion(models.Model):
	nombre = models.CharField(max_length=140)
	votos = models.IntegerField(default=0)
	def __unicode__(self):
		return self.nombre
	class Meta:
		verbose_name_plural = "Opciones"