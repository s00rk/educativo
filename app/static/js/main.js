$(document).on('ready', function (){

	$.getJSON('/api/materias/?format=json', function (data){
		$.each(data, function(i, d){
			var mundo = $('.mundo>div:eq(' + i + ')');
			if(d.nombre.length > 17)
			{
				d.nombre = d.nombre.substr(0, 13) + ' ...';
			}
			mundo.html(d.nombre);
		});
	});

	$.getJSON('/api/opciones/?format=json', function (data){
		$.each(data, function(i, d){
			var opcion = $('#opciones a:eq(' + i + ')');
			opcion.attr('id', d.id);
			opcion.html(d.nombre);
		});
	});

	$('#enviar').on('click', function(e){
		$('#formulario').submit();
	});
	$('#formulario').on('submit', function (e){
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "/api/solicitudes/",
			data: {'email': $('#email').val()},
			dataType: "json",
			success: function(data, status, xhr)
			{
				$('#formulario').parent().fadeOut();
				$('#enviado').fadeIn();
			}
		});
	});

	function getObjeto(id){
		var obj = 0;
		$.ajax({
			type: "GET",
			url: "/api/opciones/" + id,
			data: obj,
			dataType: "json",
			async: false,
			success: function(data, status, xhr)
			{
				obj = data;
			}
		});
		return obj;
	}
	$('#opciones a').on('click', function (e){
		var id = $(this).attr('id');
		var obj = getObjeto(id);
		obj.votos = obj.votos + 1;
		$.ajax({
			type: "PUT",
			url: "/api/opciones/" + id + "/",
			data: {'nombre': obj.nombre, 'votos': obj.votos},
			dataType: "json",
			success: function(data, status, xhr)
			{
				window.open('http://eachoneteachone.co/proximamente', '_blank');
				$('#modal_princ').removeClass('active');
			}
		});
	});


	$('.video_link img').on('click', function (e){
		var video = '<iframe width="283" height="150" src="//www.youtube-nocookie.com/embed/BgCWvC7D89w?&modestbranding=1&autoplay=1&showinfo=0&autohide=1&controls=0&rel=0" frameborder="0" allowfullscreen></iframe>';
		$('.video_link').html(video);
	});


});

function csrfSafeMethod(method) {
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
	var host = document.location.host;
	var protocol = document.location.protocol;
	var sr_origin = '//' + host;
	var origin = protocol + sr_origin;
	return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
		(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
		!(/^(\/\/|http:|https:).*/.test(url));
}
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
$.ajaxSetup({
	beforeSend: function(xhr, settings) {
		if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
			xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
		}
	}
});